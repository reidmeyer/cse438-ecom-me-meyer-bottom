Reid Meyer
438 Mobile Application Development
Studio 1
2/3/19

Everything done. There were a few bugs in the base code, such as in the HomeFragment.kt.

Answers to some questions:

LinearLayout: aligns objects in one direction
 vs ConstraintLayout: aligns relative to parent or child. 
 vs FrameLayout: holds one child object

What is a toast?
It's like text at the bottom of the screen.

What does it mean the function returns true or false in Oncreate/EditorActionListener?
Honestly not sure. Maybe something to do with whether the box is being interacted with.

Match_Parent: has to do with constraint layout
Wrap_content: layout options

Inflate a fragment: Means displaying by creating view object.

Role of the R object: contains the layouts